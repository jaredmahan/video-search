# VideoSearch

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

## Installation

Run `npm i`.

## Run the app

Run `ng serve` or `npm run start` for a dev server. Navigate to `http://localhost:4200/`.
