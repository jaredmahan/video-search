export default [
  {
    description:
      'Cool video about stuff and more stuff and some other stuff about stuff.',
    id: 'dQw4w9WgXcQ',
    thumbnail: 'http://placebeard.it/100x100',
    title: 'This is a long title about something long',
  },
  {
    description: 'Cool video about stuff.',
    id: 'dQw4w9WgXcQ',
    thumbnail: 'http://placebeard.it/100x100',
    title: 'Kinda Short Title',
  },
  {
    description: 'Another cool video about stuff.',
    id: 'dQw4w9WgXcQ',
    thumbnail: 'http://placebeard.it/100x100',
    title: 'Video',
  },
  {
    description: 'Really cool video about stuff.',
    id: 'dQw4w9WgXcQ',
    thumbnail: 'http://placebeard.it/100x100',
    title: 'Cool Video',
  },
  {
    description: 'Way cool video about stuff.',
    id: 'dQw4w9WgXcQ',
    thumbnail: 'http://placebeard.it/100x100',
    title: 'Videoooooooo',
  },
  {
    description: 'Amazing video about stuff and some other stuff.',
    id: 'dQw4w9WgXcQ',
    thumbnail: 'http://placebeard.it/100x100',
    title: 'Another Video',
  },
];
