export interface SearchResult {
  description: string;
  id: string;
  thumbnail: string;
  title: string;
}
