import { Component, Input, OnInit } from '@angular/core';
import { SearchResult } from '../../models/search-result.model';
import { HttpService } from '../../services/http/http.service';

@Component({
  selector: 'search-result',
  templateUrl: './search-result.component.html',
})
export class SearchResultComponent implements OnInit {
  @Input() result: SearchResult;
  commentCount: number;

  constructor(private httpService: HttpService) {}

  ngOnInit(): void {
    this.httpService.getVideoComments(this.result.id).subscribe((res: any) => {
      this.commentCount = res.items[0].statistics.commentCount;
    });
  }
}
