import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SearchResultComponent } from './search-result/search-result.component';
import { SearchResultsComponent } from './search-results/search-results.component';

@NgModule({
  declarations: [SearchResultsComponent, SearchResultComponent],
  imports: [CommonModule],
  exports: [SearchResultComponent, SearchResultsComponent],
})
export class ComponentsModule {}
