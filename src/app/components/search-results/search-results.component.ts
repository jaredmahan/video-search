import { Component, Input } from '@angular/core';
import { SearchResult } from '../../models/search-result.model';

@Component({
  selector: 'search-results',
  templateUrl: './search-results.component.html',
})
export class SearchResultsComponent {
  @Input() results: SearchResult[];
}
