import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SearchResult } from '../../models/search-result.model';
import { SearchService } from '../../services/search/search.service';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit, OnDestroy {
  results: SearchResult[];
  searchForm = new FormGroup({
    keyword: new FormControl(''),
    sortBy: new FormControl(''),
  });
  unsubscribe = new Subject();

  constructor(private searchService: SearchService) {}

  ngOnInit(): void {
    this.searchService.searchResults$
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((res: SearchResult[]) => {
        this.results = res;
      });
  }

  submit(): void {
    const keyword = this.searchForm.get('keyword').value;
    const sortBy = this.searchForm.get('sortBy').value;

    this.searchService.searchByKeyword(keyword, sortBy);

    this.searchForm.reset();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
