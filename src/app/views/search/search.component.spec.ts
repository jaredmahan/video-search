import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Subject } from 'rxjs';
import videosMock from '../../../mocks/videos.mock';
import { SearchResultsComponent } from '../../components/search-results/search-results.component';
import { SearchService } from '../../services/search/search.service';
import { SearchComponent } from './search.component';

class MockSearchService {
  searchResults$ = new Subject();
  searchByKeyword(keyword: string, sortBy: string) {
    this.searchResults$.next(videosMock);
  }
}

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let searchService: MockSearchService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchComponent, SearchResultsComponent],
      imports: [ReactiveFormsModule, FormsModule],
      providers: [{ provide: SearchService, useClass: MockSearchService }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create search component', () => {
    expect(component).toBeTruthy();
  });

  it('should have a valid form', () => {
    component.searchForm.controls['keyword'].setValue('cats');
    component.searchForm.controls['sortBy'].setValue('relevance');
    expect(component.searchForm.valid).toBeTruthy();
  });

  it('should submit search', () => {
    searchService = TestBed.inject(SearchService);
    let form = fixture.debugElement.query(By.css('form'));
    form.triggerEventHandler('submit', null);
    expect(component.results).toEqual(videosMock);
  });
});
