import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { asyncData } from '../../../testing/async-observable-helpers';
import { HttpService } from '../http/http.service';
import { SearchService } from './search.service';

describe('SearchService', () => {
  let httpClientSpy: { get: jasmine.Spy };
  let searchService: SearchService;
  let httpService: HttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientModule] });
    searchService = TestBed.inject(SearchService);
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    httpService = new HttpService(<any>httpClientSpy);
  });

  it('should be created', () => {
    expect(searchService).toBeTruthy();
  });

  it('should return expected search results (HttpClient called once)', () => {
    const expectedSearchResults: any = ['a', 'b', 'c'];

    httpClientSpy.get.and.returnValue(asyncData(expectedSearchResults));
    httpService
      .searchByKeyword('cats', 'relevance')
      .subscribe(
        (res: any) =>
          expect(res).toEqual(expectedSearchResults, 'expected search results'),
        fail
      );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
