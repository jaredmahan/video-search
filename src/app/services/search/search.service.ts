import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SearchResult } from '../../models/search-result.model';
import { HttpService } from '../http/http.service';

const result: SearchResult[] = [];

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  searchResults$ = new Subject<SearchResult[]>();

  constructor(private httpService: HttpService) {}

  private buildResults(res: any): SearchResult[] {
    return res.items.map((result: any) => {
      const id = result.id.videoId;
      const { description, thumbnails, title } = result.snippet;
      const thumbnail = thumbnails.default.url;

      return {
        description,
        id,
        thumbnail,
        title,
      };
    });
  }

  searchByKeyword(keyword: string, sortBy: string) {
    this.httpService.searchByKeyword(keyword, sortBy).subscribe((res: any) => {
      const results = this.buildResults(res);
      this.searchResults$.next(result);
    });
  }
}
