import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  // Not concerned about exposing this key for current purposes
  apiKey = 'AIzaSyDI7O_yf1CXQQVnGJQN6-ZmgfGKwxdiBZk';
  apiRoot = 'https://www.googleapis.com/youtube/v3';

  constructor(private http: HttpClient) {}

  searchByKeyword(keyword: string, sortBy: string) {
    return this.http.get(`${this.apiRoot}/search`, {
      params: {
        part: 'snippet',
        maxResults: '25',
        q: keyword,
        key: this.apiKey,
        order: sortBy,
      },
    });
  }

  getVideoComments(id: string) {
    return this.http.get(`${this.apiRoot}/videos`, {
      params: {
        part: 'statistics',
        key: this.apiKey,
        id,
      },
    });
  }
}
